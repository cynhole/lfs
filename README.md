# LunasFlightSchool
lua scripted planes for gmod. Also known as "simfphys planes"

# Installation
Put this folder into ?:\...\Steam\steamapps\common\GarrysMod\garrysmod\addons
Vehicles can then be found in your Entities tab under [LFS]. An special Missile launcher can be found in your Weapons tab under "Other"

# Controls (Default)

(outside vehicle)
E = Enter Vehicle
ALT + E = Enter closest passenger seat / gunner seat

(in vehicle)

(plane)
W = Increase Throttle
S = Decrease Throttle
A = Roll Left
D = Roll Right

R = Start Engine. Make sure you are aiming forward. There will be a signal-sound and red flashing line if your view isnt centered.

Space = Toggle Landing Gear and Flaps / Toggle S-Foils ( ARC-170 Fighter )

Shift = Pitch Up

Left Mouse Button = Primary Attack
Right Mouse Button = Secondary Attack

ALT(Hold) + Move Mouse = Free Look


Normal turning is done with the Mouse. It's mouseaim controls. However while in Free Look you can still use W A S D and Shift to maneuver.


(helicopter)
W = Increase Throttle (Thrust Up)
S = Decrease Throttle (Thrust Down)
A = Roll Left
D = Roll Right

R = Start Engine. Make sure you are aiming forward. There will be a signal-sound and red flashing line if your view isn't centered.

Shift (Hold) = Hovermode

Left Mouse Button = Primary Attack
Right Mouse Button = Secondary Attack

ALT(Hold) + Move Mouse = Free Look



# Credits

Cessna 172 Skyhawk - Original Creator Hornetnest - rigged + 2 extra skins by Luna
https://garrysmods.org/download/14222/cessna-172-skyhawk

Spitfire - Original Creator Dr. Matt & SGM - .vmt fixes, merged and rigged by Luna
https://steamcommunity.com/sharedfiles/filedetails/?id=139224513

BF 109 E-7 - Original Creator damik - texture/console error fixes and physmodel recompile by Luna (sorry damik but since i had the thing decompiled already anyway i didnt feel like bothering you again)
https://steamcommunity.com/sharedfiles/filedetails/?id=841592696